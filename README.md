# Hubot Kill 💣

hubot-kill is a [Hubot](https://hubot.github.com/) plugin to terminate the Hubot process.

## Installation

Use `npm` or your preferred package manager:

```bash
npm i hubot-kill
```

## Usage

```
hubot> hubot stop
hubot> OK, that's me out!
💥
```

## Contributing

[Contributions are welcome](https://gitlab.com/xurizaemon/hubot-kill). For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)