'use strict'

// Description:
//   Kill the hubot process.
//
// Commands:
//   hubot stop - Exit hubot (may restart, if configured to reload on exit)

module.exports = function (robot) {
  robot.respond(/(help)/i, function (msg) {
    msg.send(`${robot.name} stop - Exit the hubot process (or: go away, take a moment, stop repeating yourself)`)
  })

  robot.respond(/(stop|go away|stop repeating yourself|take a moment)/i, function (msg) {
    msg.send("OK, bye!")
    robot.logger.info('Exiting ...')
    return process.exit(0)
  })
}
